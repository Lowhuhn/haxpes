sample_thickness = 38.5000;
bend_width = 4.4099;
bend_max = 0.4122;
Voigt_mu = 0;
Voigt_sigma = 0.1545;
Voigt_gamma = 0.2640;
Voigt_area = 1;
lambda = 0.8900;
d_step = 0.0100;
E_step = 0.0100;
E_halfwidth = 10;

fprintf('peakloc_parabolic\n');
tic;
result = peakloc_parabolic(sample_thickness,bend_width, bend_max, Voigt_mu, Voigt_sigma, Voigt_gamma, Voigt_area, lambda, d_step, E_step, E_halfwidth);
toc

fprintf('new:\n');
tic;
d_val = (0:d_step:sample_thickness);
if bend_width ~=0
    E_vb = (bend_max/(bend_width^2)).*((d_val-bend_width).^2).*(d_val<bend_width);
else
    E_vb = d_val.*0;
end
P_val = exp(-d_val./lambda);
E_val=(Voigt_mu-E_halfwidth:E_step:Voigt_mu+E_halfwidth);
ii=complex(0,1);
intensities = zeros(1,length(E_val));
intP_Val = ones(1,length(E_val));
L_d = length(d_val);
parfor k = 1:L_d
   intensities = intensities + (...
                                real(cerf( (E_val-(Voigt_mu+E_vb(k))+Voigt_gamma*ii)./(sqrt(2)*Voigt_sigma)))./(Voigt_sigma*sqrt(2*pi))...
                                .* ...
                                P_val(k).*d_step);

end
data = [E_val', intensities'];
toc
   