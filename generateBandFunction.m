function [ outBandFun, shiftValue, peakX, x0, bandFunAC, bandFunABC ] = generateBandFunction(x,y)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%   Arguments:
%   - x : X Values, of the function
%   - y : Y Values, of the function
%
%     Nils Kaminski
%     IEK 5 - Photovoltaik
%     Forschungszentrum Juelich GmbH
%     D-52425 Juelich
%     Tel. +49 2461 61 2997
%     Email n.kaminski@fz-juelich.de
%
%   Generiert das Band minimal zu allen X und Y Punkten!

%% Richtiges Format f?r X
if(size(x,1)==1)
    x = x';
end

%% Richtiges Format f?r Y
if(size(y,1)==1)
    y = y';
end

%% Suche das Band mit dem minimalem Abstand zu allen Punkten
    lX = length(x);
    minBandFun = @(z) sum(abs(y-sum(bsxfun(@times, [((x-z).^2).*(x<z) , ones(lX,1)], ([((x-z).^2).*(x<z) , ones(lX,1)]\y)'), 2)));
    
    %medValue = fminsearch(minBandFun, x(1))%minValue + (maxValue-minValue)/2.0)
    peakX = fminbnd(minBandFun, min(x), max(x));%minValue + (maxValue-minValue)/2.0)

    medValueAC = [((x-peakX).^2).*(x<peakX) , ones(lX,1)]\y;
    
    shiftValue = -(medValueAC(2));
    bandFunAC = [medValueAC(1), 0];
    bandFunABC = [medValueAC(1), peakX, 0];
    
    outBandFun = @(z) bandFunAC(1).*((z-peakX).^2).*(z<peakX) + bandFunAC(2);
    x0 = outBandFun(0);
end

