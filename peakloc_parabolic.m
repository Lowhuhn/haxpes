function data=peakloc_parabolic(sample_thickness, ...
								bend_width, bend_max, ...
    							Voigt_mu, Voigt_sigma, Voigt_gamma, Voigt_area, lambda, d_step, ...
    							E_step, E_halfwidth)
% Get the electron energy peak profiles taking into account band bending 
% near the surface
% Arguments:
%  - sample_thickness in nm
%  - bend_width in nm: the depth where the bending becomes 0
%  - bend_max in eV: the energy level at the surface
%  - Voigt_mu, Voigt_sigma, Voigt_gamma, Voigt_area: parameters of the
%  Voigt profile
%  - lambda: inelastic mean free path corresponding to E_nu - E_binding
%  - d_step: distance resolution in nm
%  - E_step: energy resolution for Voigt profile in eV
%  - E_halfwidth: half the width of the range over which the Voigt profile 
%    is considered (in eV). Ensure that this is large compared to bend_max 
%    and the width of the Voigt profile peak.

% Requires cerf.m (complex error function)

    % valence band energy relative to that in the bulk
    if bend_width ~=0
        E_vb_surface = @(d) bend_max/(bend_width^2)*(d-bend_width).^2.*(d<bend_width);
    else
        E_vb_surface = @(d) 0;
    end
    
    % Voigt profile of unshifted peak
    ii=complex(0,1);
    V = @(E, delta_E) Voigt_area*...
        real(cerf((E-(Voigt_mu+delta_E) + ii*Voigt_gamma)...
        ./(sqrt(2)*Voigt_sigma)))/(Voigt_sigma*sqrt(2*pi));
    
    % Probability of not scattering as a function of depth ('intensity 
    % function')
    P = @(d,lambda) exp(-d/lambda);
    
    d_val = (0:d_step:sample_thickness);
    L_d = length(d_val);
    
    P_val = P(d_val, lambda);

    % Convolve Voigt profile with intensity function
    E_val=(Voigt_mu-E_halfwidth:E_step:Voigt_mu+E_halfwidth);
    
    intensities = zeros(1,length(E_val));
    
    for k = 1:L_d
        intensities = intensities + ...
            V(E_val, E_vb_surface(d_val(k)))*P_val(k)*d_step;
    end   

    
    data = horzcat(E_val', intensities');
   

end
  


        