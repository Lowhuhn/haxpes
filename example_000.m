%% Init the Simulation Data
% First create a data object to store the different values
sd = SimulationData();

%% Fill the Simulation Data Object with data for the simulation enviroment
sd.Iterations = 5;
sd.Verbose = 1;
sd.DispValsEachStep = 1;
sd.SaveToFile = 1;
sd.SaveToFilePath = 'dummy_result.dat';

%% Fill the Simulation Data Object with input data
% Iteration parameter, then properties for the simulation, and at least
% change some default simulation results and start data.
sd.d_step = 0.01;
sd.E_halfwidth = 10;
sd.E_step = 0.01;
sd.SampleThickness = 38.5;
sd.Voigt_mu = 0.;
sd.Voigt_area = 1;

sd.Voigt_sigma = [0.154515772, 0.216920279, 0.132562015, 0.155640354, 0.154515772, 0.194389452, 0.132562015, 0.155640354];            
sd.bend_width = 0;
sd.bend_max = 1;

sd.Cl1_IMFP = [0.89, 2.68, 4.41];
sd.Cl1_Max = [1839.584, 1839.445, 1839.32];% - 1839.584;
sd.Cl1_Voigt_Gamma = [0.887, 1.004, 0.82]./2.0;


sd.Cl2_IMFP = [4.06,5.54,7.12,10.12];
sd.Cl2_Max = [150.6097, 150.5856, 150.556, 150.4836];%-150.6097;
sd.Cl2_Voigt_Gamma = [1.156, 1.206, 1.182, 1.232] ./ 2.0 ;


%% Iterative Simulation
% Run the Simulation
% Mit dem resSD Objekt kann ab der letzten iteration weiter simuliert
% werden.
[bw,bm,vg] = iterativeSimulation(sd);