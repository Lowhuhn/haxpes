classdef SimulationData
    %SIMULATIONDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %%Properties for the iterations
        Iterations
        Verbose
        DispValsEachStep
        SaveToFile
        SaveToFilePath
        
        %%Properties for the simulation
        d_step
        E_halfwidth
        E_step
        SampleThickness
        Voigt_sigma
        Voigt_mu
        Voigt_area
        
        %Si1X
        Cl1_IMFP
        %Max or CG -> Si1Y
        Cl1_Max
        %Full Width Half Max
        Cl1_FWHM
        %Voigt gamma
        Cl1_Voigt_Gamma
        
        %Si2X
        Cl2_IMFP
        %Max or CG -> Si2Y
        Cl2_Max
        %FWHM
        Cl2_FWHM
        %Voigt gamma
        Cl2_Voigt_Gamma
        
        %%Results
        bend_width
        bend_max
        Voigt_gamma
    end
    
    methods
        function sd = SimulationData()
            %%Initialize the standard values for this class
            sd.Iterations = 1;
            sd.Verbose = 0;
            sd.DispValsEachStep =0 ;
            sd.SaveToFile = 0;
            sd.SaveToFilePath = 0;
            
            sd.d_step = 0.01;
            sd.E_halfwidth = 10;
            sd.E_step = 0.01;
            sd.SampleThickness = 38.5;
            
            sd.Voigt_sigma = [0.154515772, 0.216920279, 0.132562015, 0.155640354, 0.154515772, 0.194389452, 0.132562015, 0.155640354];
            
            sd.bend_width = 0;
            sd.bend_max = 1;
        end
        
        function obj = set.Iterations(obj, iterations)
            if(iterations <= 0)
                obj.Iterations = 0;
                error('Number of iterations must be greater than 0');
            end
            obj.Iterations = iterations;
        end
        
        function obj = set.Cl1_Max(obj, cl1max)
            obj.Cl1_Max = cl1max - cl1max(1);
        end
        function obj = set.Cl2_Max(obj, cl2max)
            obj.Cl2_Max = cl2max - cl2max(1);
        end
    end
    
end

