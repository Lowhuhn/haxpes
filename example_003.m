et2 = @(t) exp(t.^2);
myFun = @(z) exp(-z.^2).*(1+((2i)/sqrt(pi))*integral(et2, 0,z));
% //res = exp(-z.^2).*(1+((2i)/sqrt(pi))*integral(et2, 0,z));

test1 = @(v) exp(-v.^2).*(1+(1.284i)*(7.7298*erfi(v)));

xx = 1:1000;


tic;
b = cerf(xx);
toc

tic;
a = exp(-xx.^2).*(1+(1.284i)*(7.7298*erfi(xx)));
toc