function [ bend_width, bend_max, Voigt_gamma ] = iterativeSimulation( simDataObj )
% Beschreibung 
% Hier ist dein Typ verlangt David

% Turn off warnings. Most warnings were singularity warnings
warning('off');

if simDataObj.SaveToFile > 0
    op = fopen(simDataObj.SaveToFilePath, 'w');
end

verbose = simDataObj.Verbose;
verboseVars = simDataObj.DispValsEachStep;

%Init Variables
CL1N = numel(simDataObj.Cl1_IMFP);
CL2N = numel(simDataObj.Cl2_IMFP);

lambda = [simDataObj.Cl1_IMFP, simDataObj.Cl2_IMFP];
Cl12_Max = [simDataObj.Cl1_Max, simDataObj.Cl2_Max];
CL1_Max = simDataObj.Cl1_Max;
CL2_Max = simDataObj.Cl2_Max;

% CL1_Voigt_gamma = [0.35486935, 0.35453348, 0.32419866];
% CL2_Voigt_gamma = [0.50247387, 0.48598986, 0.51949226,0.52693208];
CL1_Voigt_gamma = simDataObj.Cl1_Voigt_Gamma;% [0.887, 1.004, 0.82]./2.0;
CL2_Voigt_gamma = simDataObj.Cl2_Voigt_Gamma;% [1.156, 1.206, 1.182, 1.232] ./ 2.0 ;
CL12Voigt_gamma = [CL1_Voigt_gamma, CL2_Voigt_gamma];


%Nehme jeweils den kleinsten voigt gamma wert aus cl1 und cl2
%Voigt_gamma = [ones(1,length(simDataObj.Cl1_IMFP)).*(CL1_Voigt_gamma(3)), ones(1,length(simDataObj.Cl2_IMFP)).*(CL2_Voigt_gamma(1))];
Voigt_gamma = [ones(1,length(simDataObj.Cl1_IMFP)).*(min(CL1_Voigt_gamma)), ones(1,length(simDataObj.Cl2_IMFP)).*(min(CL2_Voigt_gamma))];

Voigt_mu = simDataObj.Voigt_mu;
Voigt_area = simDataObj.Voigt_area;
shiftValue = 0;
bend_width = simDataObj.bend_width;
bend_max = simDataObj.bend_max;

%Make the first Fit
if verbose > 0 
    fprintf('Iteration: 0\n');
end
[ofun, shiftValue, bend_width, bend_max] = generateBandFunction(simDataObj.Cl1_IMFP,simDataObj.Cl1_Max); 



CL2_Max= CL2_Max - sum(CL2_Max - (ofun(simDataObj.Cl2_IMFP)-shiftValue))/length(CL2_Max);
Cl12_Max = [CL1_Max, CL2_Max]+shiftValue;
CL2_Max = CL2_Max+shiftValue;

if simDataObj.SaveToFile > 0
    fprintf(op, '%d \t %s %s %s %s %1.10e \t %1.10e \t %1.10e \n',0,  sprintf('%1.10e\t', CL12Voigt_gamma), ...
                                                               sprintf('%1.10e\t', Voigt_gamma),...
                                                               sprintf('%1.10e\t', CL1_Max), ...
                                                               sprintf('%1.10e\t', CL2_Max), ...
                                                               shiftValue, bend_width,bend_max);
end

n = length(lambda);
res = ones(1,n);
gamma = ones(1,n);
for iter = 1:simDataObj.Iterations
    if verbose > 0 
        fprintf('Iteration: %d\t', iter);
        tic;
    end
    
    for v = 1:n
        result = peakloc_parabolic_fast(simDataObj.SampleThickness, bend_width, bend_max, ...
                                        Voigt_mu, simDataObj.Voigt_sigma(v), Voigt_gamma(v), Voigt_area, ...
                                        lambda(v), simDataObj.d_step, simDataObj.E_step, simDataObj.E_halfwidth);
                                    
        %!Optional Kurve durch result punkte und maximum der Kurve bestimmen
        [maxVal,index] = max(result(:,2));
        res(v) = result(index);     
        
        %Berechne fuer jeden Peak die max pos und die breite auf halber hoehe
        %! 
        hh = maxVal / 2.0;
        lr = find(result(: ,2) >= hh);
        fwhm = sqrt( (result(lr(1),1) - result(lr(end),1))^2);
                
        fg = 2*simDataObj.Voigt_sigma(v)*sqrt(2*log(2));        
        gamma(v) = ( -sqrt( (0.5346^2 * fg^2) - (0.2166*fg^2) + (0.2166*fwhm^2) ) + 0.5346*fwhm) / (2*(0.5346^2-0.2166));
        
        if verbose > 0 
            fprintf('|');
        end
    end
    
    
    Voigt_gamma(1:CL1N) = Voigt_gamma(1:CL1N) - (sum(gamma(1:CL1N) - CL1_Voigt_gamma)/CL1N);
    Voigt_gamma(CL1N+1:end) = Voigt_gamma(CL1N+1:end) - (sum(gamma(CL1N+1:end) - CL2_Voigt_gamma)/CL2N);

	CL2_Max = CL2_Max-res(4:end)+ofun(simDataObj.Cl2_IMFP);

    [ofun, shiftValue, bend_width, bend_max] = generateBandFunction(lambda, (Cl12_Max-res)+ofun(lambda));
    Cl12_Max = Cl12_Max + shiftValue;
    
    Cl12_Max(4:end) =  Cl12_Max(4:end) - sum(CL2_Max - (ofun(simDataObj.Cl2_IMFP)-shiftValue))/length(CL2_Max);
    CL2_Max = Cl12_Max(4:end);
    
    if verbose > 0 
        time = toc;
        if verboseVars > 0
            fprintf('\t bend width: %f\t bend max: %f', bend_width, bend_max);
        end
        fprintf('\t Time: %f\n', time);        
        
    end
    
    if simDataObj.SaveToFile > 0
        fprintf(op, '%d \t %s %s %s %s %1.10e \t %1.10e \t %1.10e \n',iter,  sprintf('%1.10e\t', CL12Voigt_gamma), ...
                                                               sprintf('%1.10e\t', Voigt_gamma),...
                                                               sprintf('%1.10e\t', CL1_Max), ...
                                                               sprintf('%1.10e\t', CL2_Max), ...
                                                               shiftValue, bend_width,bend_max);
    end
end

if verbose
    Voigt_gamma
    bend_width
    bend_max
end

if simDataObj.SaveToFile > 0
    fclose(op);
end

end

